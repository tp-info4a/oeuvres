# TP Oeuvre

## Auteurs

CAZET Martin & ARMANET Nathan

## Contexte

Ce projet a été réalisé dans le cadre de l'UE `Informatique Répartie` du cursus de Polytech Lyon pour la 4A INFO

## Explications

### 1. Les services

Une classe abstraite [Service](./src/main/java/com/epul/oeuvres/dao/Service.java) qui utilise une classe générique `Entity`.
Cette classe générique fait références aux entités de la base de données.

Elle a été créée pour éviter la répétition de code pour l'insertion, la suppréssion, la sélection de toutes les
occurences de cette entité et la sélection d'une seule occurence sélectionner grâce à son identifiant.