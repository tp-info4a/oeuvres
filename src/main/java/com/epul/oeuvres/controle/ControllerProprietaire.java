package com.epul.oeuvres.controle;

import com.epul.oeuvres.dao.ServiceProprietaire;
import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.Proprietaire;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ControllerProprietaire {
    private ServiceProprietaire serviceProprietaire;

    public ControllerProprietaire(ServiceProprietaire serviceProprietaire) {
        this.serviceProprietaire = serviceProprietaire;
    }

    @RequestMapping(value = "listerProprietaire.htm", method = RequestMethod.GET)
    public ModelAndView afficherLesStages(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String destinationPage;
        try {
            // HttpSession session = request.getSession();
            request.setAttribute("proprietaireList", this.serviceProprietaire.getAll());
            destinationPage = "vues/listeProprietaire";
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";

        }
        return new ModelAndView(destinationPage);
    }

    @RequestMapping(value = "supprimerProprietaire.htm", method = RequestMethod.GET)
    public ModelAndView supprimerProprietaire(HttpServletRequest request, HttpServletResponse response) {
        try {
            this.serviceProprietaire.delete(Integer.parseInt(request.getParameter("id")));
            return new ModelAndView(new RedirectView("listerProprietaire.htm"));
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }
    }

    @RequestMapping(value = "updateProprietaire.htm", method = RequestMethod.GET)
    public ModelAndView updateProprietaire(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameterMap().containsKey("id")) {
                request.setAttribute("proprietaire", this.serviceProprietaire.getById(Integer.parseInt(request.getParameter("id"))));
            } else {
                request.setAttribute("proprietaire", new Proprietaire());
            }
        }catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }
        return new ModelAndView("vues/ajouterProprietaire");
    }

    @RequestMapping(value = "insererProprietaire.htm", method = RequestMethod.POST)
    public ModelAndView insererProprietaire(HttpServletRequest request, HttpServletResponse response) {
        try {
            Proprietaire proprietaire = new Proprietaire();
            proprietaire.setId(Integer.parseInt(request.getParameter("id")));
            proprietaire.setNom(request.getParameter("txtnom"));
            proprietaire.setPrenom(request.getParameter("txtprenom"));
            if (proprietaire.getId() == 0) {
                this.serviceProprietaire.insert(proprietaire);
            } else {
                this.serviceProprietaire.update(proprietaire);
            }

        } catch (Exception e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("vues/Erreur");
        }
        return new ModelAndView(new RedirectView("listerProprietaire.htm"));
    }
}
