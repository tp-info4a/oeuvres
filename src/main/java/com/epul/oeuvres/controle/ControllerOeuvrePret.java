package com.epul.oeuvres.controle;

import com.epul.oeuvres.dao.ServiceOeuvrePret;
import com.epul.oeuvres.dao.ServiceProprietaire;
import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.OeuvrePret;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ControllerOeuvrePret {

    private ServiceProprietaire serviceProprietaire;
    private ServiceOeuvrePret serviceOeuvrePret;

    public ControllerOeuvrePret(ServiceProprietaire serviceProprietaire, ServiceOeuvrePret serviceOeuvrePret) {
        this.serviceProprietaire = serviceProprietaire;
        this.serviceOeuvrePret = serviceOeuvrePret;
    }

    @RequestMapping(value = "listeOeuvrePret.htm", method = RequestMethod.GET)
    public ModelAndView listeOeuvrePret(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("oeuvrePretList", serviceOeuvrePret.getAllOrderByTitre());
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }

        return new ModelAndView("vues/listeOeuvrePret");
    }

    @RequestMapping(value = "supprimerOeuvrePret.htm", method = RequestMethod.GET)
    public ModelAndView supprimerOeuvrePret(HttpServletRequest request, HttpServletResponse response) {
        try {
            serviceOeuvrePret.delete(Integer.parseInt(request.getParameter("id")));
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }

        return new ModelAndView(new RedirectView("listeOeuvrePret.htm"));
    }

    @RequestMapping(value = "updateOeuvrePret.htm", method = RequestMethod.GET)
    public ModelAndView updateOeuvrePret(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameterMap().containsKey("id")) {
                request.setAttribute("oeuvrePret", serviceOeuvrePret.getById(Integer.parseInt(request.getParameter("id"))));
            } else {
                request.setAttribute("oeuvrePret", new OeuvrePret());
            }
            request.setAttribute("proprietaireList", serviceProprietaire.getAll());
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }
        return new ModelAndView("vues/ajouterOeuvrePret");
    }

    @RequestMapping(value = "insererOeuvrePret.htm", method = RequestMethod.POST)
    public ModelAndView insererOeuvrePret(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameterMap().containsKey("id") && Integer.parseInt(request.getParameter("id")) > 0) {
                serviceOeuvrePret.updateOeuvrePret(Integer.parseInt(
                        request.getParameter("id")),
                        request.getParameter("titre"),
                        serviceProprietaire.getById(Integer.parseInt(request.getParameter("proprietaire")))
                    );
            }else {
                OeuvrePret oeuvrePret = new OeuvrePret();
                oeuvrePret.setTitre(request.getParameter("titre"));
                oeuvrePret.setProprietaire(serviceProprietaire.getById(Integer.parseInt(request.getParameter("proprietaire"))));

                serviceOeuvrePret.insert(oeuvrePret);
            }
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }

        return new ModelAndView(new RedirectView("listeOeuvrePret.htm"));
    }
}
