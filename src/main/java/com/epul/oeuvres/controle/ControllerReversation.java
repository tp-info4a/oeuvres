package com.epul.oeuvres.controle;

import com.epul.oeuvres.dao.ServiceAdherent;
import com.epul.oeuvres.dao.ServiceOeuvreVente;
import com.epul.oeuvres.dao.ServiceReservation;
import com.epul.oeuvres.metier.Adherent;
import com.epul.oeuvres.metier.OeuvreVente;
import com.epul.oeuvres.metier.ReservationStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ControllerReversation {

    private ServiceOeuvreVente serviceOeuvreVente;
    private ServiceAdherent serviceAdherent;
    private ServiceReservation serviceReservation;

    public ControllerReversation(ServiceOeuvreVente serviceOeuvreVente, ServiceAdherent serviceAdherent, ServiceReservation serviceReservation) {
        this.serviceOeuvreVente = serviceOeuvreVente;
        this.serviceAdherent = serviceAdherent;
        this.serviceReservation = serviceReservation;
    }

    @RequestMapping(value = "reservationOeuvre.htm", method = RequestMethod.GET)
    public ModelAndView formReservation(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("oeuvreVente", serviceOeuvreVente.getById(Integer.parseInt(request.getParameter("id"))));
            request.setAttribute("adherentList", serviceAdherent.getAllOrderByNom());
            request.setAttribute("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        } catch (Exception e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("vues/Erreur");
        }

        return new ModelAndView("vues/ajouterReservation");
    }

    @RequestMapping(value = "insererReservation.htm", method = RequestMethod.POST)
    public ModelAndView insererReservation(HttpServletRequest request, HttpServletResponse response) {
        try {
            OeuvreVente oeuvreVente = serviceOeuvreVente.getById(Integer.parseInt(request.getParameter("idOeuvreVente")));
            Adherent adherent = serviceAdherent.getById(Integer.parseInt(request.getParameter("idAdherent")));
            serviceReservation.insererReservation(oeuvreVente, request.getParameter("date"), adherent);
        } catch (Exception e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("vues/Erreur");
        }
        return new ModelAndView(new RedirectView("listerOeuvreVente.htm"));
    }

    @RequestMapping(value = "confirmReservation.htm", method = RequestMethod.GET)
    public ModelAndView confirmReservation(HttpServletRequest request, HttpServletResponse response) {
        try {
            OeuvreVente oeuvreVente = serviceOeuvreVente.getById(Integer.parseInt(request.getParameter("idO")));
            Adherent adherent = serviceAdherent.getById(Integer.parseInt(request.getParameter("idA")));
            ReservationStatus status = ReservationStatus.valueOf(request.getParameter("status"));
            serviceReservation.updateReservation(oeuvreVente, adherent, status);
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("vues/Erreur");
        }
        return new ModelAndView(new RedirectView("listerOeuvreVente.htm"));
    }
}
