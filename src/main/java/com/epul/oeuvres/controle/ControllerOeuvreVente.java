package com.epul.oeuvres.controle;

import com.epul.oeuvres.dao.ServiceOeuvreVente;
import com.epul.oeuvres.dao.ServiceProprietaire;
import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.OeuvreVente;
import com.epul.oeuvres.metier.OeuvreVenteEtat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ControllerOeuvreVente {

    private ServiceOeuvreVente serviceOeuvreVente;
    private ServiceProprietaire serviceProprietaire;

    public ControllerOeuvreVente(ServiceOeuvreVente serviceOeuvreVente, ServiceProprietaire serviceProprietaire) {
        this.serviceOeuvreVente = serviceOeuvreVente;
        this.serviceProprietaire = serviceProprietaire;
    }

    @RequestMapping(value = "listerOeuvreVente.htm", method = RequestMethod.GET)
    public ModelAndView listeOeuvreVente(HttpServletRequest request, HttpServletResponse response) {
        String destinationPage;
        try {
            request.setAttribute("oeuvreVenteList", serviceOeuvreVente.getAllOrderByTitre());
            destinationPage = "vues/listeOeuvreVente";
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";

        }
        return new ModelAndView(destinationPage);
    }

    @RequestMapping(value = "supprimerOeuvreVente.htm", method = RequestMethod.GET)
    public ModelAndView supprimerOeuvreVente(HttpServletRequest request, HttpServletResponse response) {
        try {
            serviceOeuvreVente.delete(Integer.parseInt(request.getParameter("id")));
            return new ModelAndView(new RedirectView("listerOeuvreVente.htm"));
        } catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }

    }

    @RequestMapping(value = "updateOeuvreVente.htm", method = RequestMethod.GET)
    public ModelAndView updateOeuvreVente(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getParameterMap().containsKey("id")) {
                request.setAttribute("oeuvreVente", serviceOeuvreVente.getById(Integer.parseInt(request.getParameter("id"))));
            } else {
                request.setAttribute("oeuvreVente", new OeuvreVente());
            }
            request.setAttribute("proprietaireList", serviceProprietaire.getAll());
        }catch (MonException e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("Erreur");
        }
        return new ModelAndView("vues/ajouterOeuvreVente");
    }

    @RequestMapping(value = "insererOeuvreVente.htm", method = RequestMethod.POST)
    public ModelAndView insererOeuvreVente(HttpServletRequest request, HttpServletResponse response) {
        try {
            OeuvreVente oeuvreVente = new OeuvreVente();
            oeuvreVente.setId(Integer.parseInt(request.getParameter("id")));
            oeuvreVente.setTitre(request.getParameter("titre"));
            oeuvreVente.setEtat(OeuvreVenteEtat.LIBRE.getEtat());
            oeuvreVente.setPrix(Double.parseDouble(request.getParameter("prix")));
            oeuvreVente.setProprietaire(serviceProprietaire.getById(Integer.parseInt(request.getParameter("proprietaire"))));
            if (oeuvreVente.getId() == 0) {
                oeuvreVente.setEtat("L");
                serviceOeuvreVente.insert(oeuvreVente);
            } else {
                serviceOeuvreVente.updateOeuvreVente(oeuvreVente);
            }

        } catch (Exception e) {
            request.setAttribute("MesErreurs", e.getMessage());
            return new ModelAndView("vues/Erreur");
        }
        return new ModelAndView(new RedirectView("listerOeuvreVente.htm"));
    }
}
