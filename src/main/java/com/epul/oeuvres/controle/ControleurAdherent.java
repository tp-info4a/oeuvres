package com.epul.oeuvres.controle;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.epul.oeuvres.dao.ServiceAdherent;
import com.epul.oeuvres.exceptions.*;
import com.epul.oeuvres.metier.*;
import org.springframework.web.servlet.view.RedirectView;

///
/// Les m�thode du contr�leur r�pondent � des sollicitations
/// des pages JSP

@Controller("/adherent")
public class ControleurAdherent {

//	private static final Logger logger = LoggerFactory.getLogger(MultiControleur.class);

	private ServiceAdherent serviceAdherent;

	public ControleurAdherent(ServiceAdherent serviceAdherent) {
		this.serviceAdherent = serviceAdherent;
	}

	@RequestMapping(value = "listerAdherent.htm", method = RequestMethod.GET)
	public ModelAndView afficherLesStages(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String destinationPage;
		try {
			// HttpSession session = request.getSession();
			request.setAttribute("mesAdherents", this.serviceAdherent.getAllOrderByNom());
			destinationPage = "vues/listeAdherent";
		} catch (MonException e) {
			request.setAttribute("MesErreurs", e.getMessage());
			destinationPage = "Erreur";

		}
		return new ModelAndView(destinationPage);
	}



	@RequestMapping(value = "insererAdherent.htm", method = RequestMethod.POST)
	public ModelAndView insererAdherent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			Adherent unAdherent = new Adherent();
			unAdherent.setId(Integer.parseInt(request.getParameter("id")));
			unAdherent.setNom(request.getParameter("txtnom"));
			unAdherent.setPrenom(request.getParameter("txtprenom"));
			unAdherent.setVille(request.getParameter("txtville"));
			if (unAdherent.getId() == 0)
				this.serviceAdherent.insert(unAdherent);
			else
				this.serviceAdherent.update(unAdherent);
		} catch (Exception e) {
			request.setAttribute("MesErreurs", e.getMessage());

			return new ModelAndView("vues/Erreur");
		}

		return new ModelAndView(new RedirectView("listerAdherent.htm"));
	}

	@RequestMapping(value = "updateAdherent.htm", method = RequestMethod.GET)
	public ModelAndView updateAdherent(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String destinationPage = "";
		try {
			if (request.getParameterMap().containsKey("id")) {
				request.setAttribute("adherent", serviceAdherent.getById(Integer.parseInt(request.getParameter("id"))));
			} else {
				request.setAttribute("adherent", new Adherent());
			}
			destinationPage = "vues/ajouterAdherent";
		} catch (Exception e) {
			request.setAttribute("MesErreurs", e.getMessage());
			destinationPage = "vues/Erreur";
		}

		return new ModelAndView(destinationPage);
	}

	@RequestMapping(value = "supprimerAdherent.htm", method = RequestMethod.GET)
	public ModelAndView supprimerAdherent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			this.serviceAdherent.delete(Integer.parseInt(request.getParameter("id")));
			return new ModelAndView(new RedirectView("listerAdherent.htm"));
		} catch (Exception e) {
			request.setAttribute("MesErreurs", e.getMessage());
		}

		return new ModelAndView("vues/Erreur");
	}



	// /
	// / Affichage de la page d'accueil
	// /
	@RequestMapping(value = "index.htm", method = RequestMethod.GET)
	public ModelAndView Afficheindex(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("index");
	}

	// /
	// / Affichage de la page d'accueil
	// /
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView Afficheindex2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("index");
	}

	// /
	// / Affichage de la page d'accueil
	// /
	@RequestMapping(value = "erreur.htm", method = RequestMethod.GET)
	public ModelAndView AfficheErreur(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("vues/Erreur");
	}
}
