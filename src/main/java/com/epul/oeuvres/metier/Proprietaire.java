package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by christian on 19/02/2017.
 */
@Entity
@Table(name = "proprietaire", schema = "baseoeuvre", catalog = "")
public class Proprietaire implements Serializable {
    private int id;
    private String nom;
    private String prenom;

    @Id
    @Column(name = "id_proprietaire")
    public int getId() {
        return id;
    }

    public void setId(int idProprietaire) {
        this.id = idProprietaire;
    }

    @Basic
    @Column(name = "nom_proprietaire")
    public String getNom() {
        return nom;
    }

    public void setNom(String nomProprietaire) {
        this.nom = nomProprietaire;
    }

    @Basic
    @Column(name = "prenom_proprietaire")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenomProprietaire) {
        this.prenom = prenomProprietaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Proprietaire that = (Proprietaire) o;

        if (id != that.id) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null)
            return false;
        if (prenom != null ? !prenom.equals(that.prenom) : that.prenom != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        return result;
    }
}
