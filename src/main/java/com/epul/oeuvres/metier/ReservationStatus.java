package com.epul.oeuvres.metier;

public enum ReservationStatus {
    CONFIRMED,
    WAITING,
    CANCELLED
}
