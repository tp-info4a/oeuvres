package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by christian on 19/02/2017.
 */
@Entity
@Table(name = "oeuvrevente", schema = "baseoeuvre")
public class OeuvreVente implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_oeuvrevente")
    private int id;

    @Basic
    @Column(name = "titre_oeuvrevente")
    private String titre;

    @Basic
    @Column(name = "etat_oeuvrevente")
    private String etat;

    @Basic
    @Column(name = "prix_oeuvrevente")
    private double prix;

    @ManyToOne()
    @JoinColumn(name = "id_proprietaire")
    private Proprietaire proprietaire;

    @OneToMany(mappedBy = "id.oeuvreVente", orphanRemoval = true, cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<Reservation> reservationList;

    public int getId() {
        return id;
    }

    public void setId(int idOeuvrevente) {
        this.id = idOeuvrevente;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titreOeuvrevente) {
        this.titre = titreOeuvrevente;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etatOeuvrevente) {
        this.etat = etatOeuvrevente;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prixOeuvrevente) {
        this.prix = prixOeuvrevente;
    }

    public Proprietaire getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Proprietaire proprietaire) {
        this.proprietaire = proprietaire;
    }

    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OeuvreVente that = (OeuvreVente) o;

        if (id != that.id) return false;
        if (Double.compare(that.prix, prix) != 0) return false;
        if (titre != null ? !titre.equals(that.titre) : that.titre != null)
            return false;
        if (etat != null ? !etat.equals(that.etat) : that.etat != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (titre != null ? titre.hashCode() : 0);
        result = 31 * result + (etat != null ? etat.hashCode() : 0);
        temp = Double.doubleToLongBits(prix);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
