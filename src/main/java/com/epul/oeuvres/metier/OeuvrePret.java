package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by christian on 19/02/2017.
 */
@Entity
@Table(name = "oeuvrepret", schema = "baseoeuvre")
public class OeuvrePret {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_oeuvrepret")
    private int id;

    @Basic
    @Column(name = "titre_oeuvrepret")
    private String titre;

    @ManyToOne
    @JoinColumn(name = "id_proprietaire")
    private Proprietaire proprietaire;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Proprietaire getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Proprietaire proprietaire) {
        this.proprietaire = proprietaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OeuvrePret that = (OeuvrePret) o;
        return id == that.id &&
                titre.equals(that.titre) &&
                proprietaire.equals(that.proprietaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titre, proprietaire);
    }
}
