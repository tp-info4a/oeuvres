package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by christian on 19/02/2017.
 */
@Entity
@Table(name = "adherent", schema = "baseoeuvre")
public class Adherent implements Serializable {
    private int id;
    private String nom;
    private String prenom;
    private String ville;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_adherent")
    public int getId() {
        return id;
    }

    public void setId(int idAdherent) {
        this.id = idAdherent;
    }

    @Basic
    @Column(name = "nom_adherent")
    public String getNom() {
        return nom;
    }

    public void setNom(String nomAdherent) {
        this.nom = nomAdherent;
    }

    @Basic
    @Column(name = "prenom_adherent")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenomAdherent) {
        this.prenom = prenomAdherent;
    }

    @Basic
    @Column(name = "ville_adherent")
    public String getVille() {
        return ville;
    }

    public void setVille(String villeAdherent) {
        this.ville = villeAdherent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Adherent that = (Adherent) o;

        if (id != that.id) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (prenom != null ? !prenom.equals(that.prenom) : that.prenom != null)
            return false;
        if (ville != null ? !ville.equals(that.ville) : that.ville != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        return result;
    }
}
