package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

/**
 * Created by christian on 19/02/2017.
 */
@Entity
@Table(name = "reservation", schema = "baseoeuvre")
public class Reservation implements Serializable {
    @EmbeddedId
    private ReservationPK id;

    @Basic
    @Column(name = "date_reservation")
    private Date date_reservation;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut", columnDefinition = "varchar", length = 20)
    private ReservationStatus statut;

    public ReservationPK getId() {
        return id;
    }

    public void setId(ReservationPK id) {
        this.id = id;
    }

    public Date getDate_reservation() {
        return date_reservation;
    }

    public void setDate_reservation(Date dateReservation) {
        this.date_reservation = dateReservation;
    }

    public ReservationStatus getStatut() {
        return statut;
    }

    public void setStatut(ReservationStatus statut) {
        this.statut = statut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return id.equals(that.id) &&
                date_reservation.equals(that.date_reservation) &&
                statut == that.statut;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date_reservation, statut);
    }
}
