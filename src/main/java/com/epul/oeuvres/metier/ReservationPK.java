package com.epul.oeuvres.metier;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by christian on 19/02/2017.
 */
@Embeddable
public class ReservationPK implements Serializable {
    @ManyToOne
    @JoinColumn(name = "id_oeuvrevente", nullable = false)
    private OeuvreVente oeuvreVente;

    @ManyToOne
    @JoinColumn(name = "id_adherent", nullable = false)
    private Adherent adherent;

    public ReservationPK() {}

    public ReservationPK(OeuvreVente oeuvreVente, Adherent adherent) {
        this.oeuvreVente = oeuvreVente;
        this.adherent = adherent;
    }

    public OeuvreVente getOeuvreVente() {
        return this.oeuvreVente;
    }

    public void setOeuvreVente(OeuvreVente oeuvreVente) {
        this.oeuvreVente = oeuvreVente;
    }

    public Adherent getAdherent() {
        return this.adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationPK that = (ReservationPK) o;
        return Objects.equals(oeuvreVente, that.oeuvreVente) &&
                Objects.equals(adherent, that.adherent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oeuvreVente, adherent);
    }
}
