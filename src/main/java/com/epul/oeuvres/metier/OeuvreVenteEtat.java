package com.epul.oeuvres.metier;

public enum OeuvreVenteEtat {
    LIBRE("L"),
    RESERVE("R");

    private String etat;

    OeuvreVenteEtat(String etat) {
        this.etat = etat;
    }

    public String getEtat() {
        return etat;
    }
}
