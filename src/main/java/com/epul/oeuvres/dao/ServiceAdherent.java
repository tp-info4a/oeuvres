package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.Adherent;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import java.util.Comparator;
import java.util.List;

@Service
public class ServiceAdherent extends com.epul.oeuvres.dao.Service<Adherent> {

	public ServiceAdherent() {
		super(Adherent.class);
	}

	/* Lister les adherents
	 * */
	public List<Adherent> getAllOrderByNom() throws MonException {
		List<Adherent> mesAdherents = this.getAll();
		mesAdherents.sort(Comparator.comparing(Adherent::getNom));
		return mesAdherents;
	}

	public void update(Adherent unAdherent) throws MonException {
		try {
			EntityTransaction entityTransaction = startTransaction();
			entityTransaction.begin();
			Adherent adherent = entitymanager.find(Adherent.class, unAdherent.getId());
			adherent.setNom(unAdherent.getNom());
			adherent.setPrenom(unAdherent.getPrenom());
			adherent.setVille(unAdherent.getVille());
			entityTransaction.commit();
		} catch (Exception e) {
			throw new MonException("erreur", e.getMessage());
		}
	}
}
