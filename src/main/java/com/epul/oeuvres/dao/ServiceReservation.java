package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.*;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import java.sql.Date;

@Service
public class ServiceReservation extends EntityService {

    public void insererReservation(OeuvreVente oeuvreVente, String date, Adherent adherent) throws MonException {
        Reservation reservation = new Reservation();
        try {
            reservation.setId(new ReservationPK(oeuvreVente, adherent));
            reservation.setDate_reservation(Date.valueOf(date));
            reservation.setStatut(ReservationStatus.WAITING);

            EntityTransaction entityTransaction = startTransaction();
            entityTransaction.begin();
            entitymanager.persist(reservation);
            OeuvreVente oeuvreVenteBDD = entitymanager.find(OeuvreVente.class, oeuvreVente.getId());
            oeuvreVenteBDD.setEtat(OeuvreVenteEtat.RESERVE.getEtat());
            entityTransaction.commit();
        } catch (Exception e) {
            throw new MonException("erreur", e.getMessage());
        }
    }

    public void updateReservation(OeuvreVente oeuvreVente, Adherent adherent, ReservationStatus status) throws MonException {
        try {
            EntityTransaction entityTransaction = startTransaction();
            entityTransaction.begin();
            Reservation reservation = entitymanager.find(Reservation.class, new ReservationPK(oeuvreVente, adherent));
            reservation.setStatut(status);
            if (status.compareTo(ReservationStatus.CANCELLED) == 0) {
                OeuvreVente oeuvreVente1 = entitymanager.find(OeuvreVente.class, oeuvreVente.getId());
                oeuvreVente1.setEtat(OeuvreVenteEtat.LIBRE.getEtat());
            }
            entityTransaction.commit();
        } catch (Exception e) {
            throw new MonException("erreur", e.getMessage());
        }
    }
}
