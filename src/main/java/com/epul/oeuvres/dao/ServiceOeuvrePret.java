package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.OeuvrePret;
import com.epul.oeuvres.metier.Proprietaire;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import java.util.Comparator;
import java.util.List;

@Service
public class ServiceOeuvrePret extends com.epul.oeuvres.dao.Service<OeuvrePret> {

    public ServiceOeuvrePret() {
        super(OeuvrePret.class);
    }

    public List<OeuvrePret> getAllOrderByTitre() throws MonException {
        List<OeuvrePret> list = this.getAll();
        list.sort(Comparator.comparing(OeuvrePret::getTitre));
        return list;
    }



    public void updateOeuvrePret(int id, String titre, Proprietaire proprietaire) throws MonException {
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();
            OeuvrePret oeuvrePret = entitymanager.find(OeuvrePret.class, id);
            oeuvrePret.setTitre(titre);
            oeuvrePret.setProprietaire(proprietaire);
            transac.commit();
        } catch (Exception e) {
            throw new MonException("Erreur", e.getMessage());
        }
    }
}
