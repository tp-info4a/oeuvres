package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;

import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class Service<Entity> extends EntityService {

    private Class<Entity> entityClass;

    // implement entityClass with Entity.class
    // example : for ServiceAdherent for Adherent entity, this.entityClass = Adherent.class
    public Service(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }

    // Add an entity in database
    public void insert(Entity entity) throws MonException {
        try
        {
            EntityTransaction transac = startTransaction();
            transac.begin();
            entitymanager.persist(entity);
            transac.commit();
            entitymanager.close();
        } catch (Exception e) {
            throw new MonException("Erreur de lecture", e.getMessage());
        }
    }

    // Get all entities
    public List<Entity> getAll() throws MonException {
        List<Entity> tList;
        try {
            EntityTransaction transaction = startTransaction();
            transaction.begin();
            CriteriaBuilder criteriaBuilder = entitymanager.getCriteriaBuilder();
            CriteriaQuery<Entity> query = criteriaBuilder.createQuery(this.entityClass);
            Root<Entity> from = query.from(this.entityClass);
            CriteriaQuery<Entity> select = query.select(from);
            tList = entitymanager.createQuery(select).getResultList();
        } catch (Exception e) {
            throw new MonException("Erreur de lecture", e.getMessage());
        }

        return tList;
    }

    // Get an entity from database with its id
    public Entity getById(int id) throws MonException {
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();
            Entity entity = entitymanager.find(this.entityClass, id);
            transac.commit();
            return entity;
        } catch (Exception e) {
            throw new MonException("Erreur", e.getMessage());
        }
    }

    // Delete an entity from database
    public void delete(int id) throws MonException {
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();
            entitymanager.remove(entitymanager.find(this.entityClass, id));
            transac.commit();
        } catch (Exception e) {
            throw new MonException("Erreur", e.getMessage());
        }
    }
}
