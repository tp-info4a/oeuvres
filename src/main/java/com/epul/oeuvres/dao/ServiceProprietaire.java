package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.Proprietaire;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;

@Service
public class ServiceProprietaire extends com.epul.oeuvres.dao.Service<Proprietaire> {

    public ServiceProprietaire() {
        super(Proprietaire.class);
    }

    public void update(Proprietaire proprietaire) throws MonException {
        try {
            EntityTransaction entityTransaction = startTransaction();
            entityTransaction.begin();
            Proprietaire proprietaireBDD = entitymanager.find(Proprietaire.class, proprietaire.getId());
            proprietaireBDD.setNom(proprietaire.getNom());
            proprietaireBDD.setPrenom(proprietaire.getPrenom());
            entityTransaction.commit();
        } catch (Exception e) {
            throw new MonException("erreur", e.getMessage());
        }
    }
}
