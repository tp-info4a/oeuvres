package com.epul.oeuvres.dao;


import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.OeuvreVente;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import java.util.Comparator;
import java.util.List;

@Service
public class ServiceOeuvreVente extends com.epul.oeuvres.dao.Service<OeuvreVente> {

    public ServiceOeuvreVente() {
        super(OeuvreVente.class);
    }

    public List<OeuvreVente> getAllOrderByTitre() throws MonException {
        List<OeuvreVente> oeuvreVenteList = this.getAll();
        oeuvreVenteList.sort(Comparator.comparing(OeuvreVente::getTitre));
        return oeuvreVenteList;
    }

    public void updateOeuvreVente(OeuvreVente oeuvreVente) throws MonException {
        try {
            EntityTransaction entityTransaction = startTransaction();
            entityTransaction.begin();
            OeuvreVente oeuvreBDD = entitymanager.find(OeuvreVente.class, oeuvreVente.getId());
            oeuvreBDD.setTitre(oeuvreVente.getTitre());
            oeuvreBDD.setProprietaire(oeuvreVente.getProprietaire());
            oeuvreBDD.setPrix(oeuvreVente.getPrix());
            entityTransaction.commit();
        } catch (Exception e) {
            throw new MonException("erreur", e.getMessage());
        }
    }
}
