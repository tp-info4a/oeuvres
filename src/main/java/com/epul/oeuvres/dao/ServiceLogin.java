package com.epul.oeuvres.dao;

import com.epul.oeuvres.exceptions.MonException;
import com.epul.oeuvres.metier.Utilisateur;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class ServiceLogin  extends EntityService {


    public Utilisateur getUtilisateur(String login) throws MonException {
        Utilisateur utilisateur;
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();

            Query query = entitymanager.createNamedQuery("UtilisateurEntity.rechercheNom");
            query.setParameter("name", login);
            utilisateur = (Utilisateur) query.getSingleResult();
            if (utilisateur == null) {
                throw new MonException("Utilisateur Inconnu", "Erreur ");
            }
            entitymanager.close();

        } catch (MonException e) {
            throw e;
        } catch(Exception e) {
            throw new MonException("Erreur de lecture", e.getMessage());
        }
        return utilisateur;
    }

}



