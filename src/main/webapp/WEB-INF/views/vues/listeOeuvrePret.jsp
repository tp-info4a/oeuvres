<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<%@include file="header.jsp" %>
<body>
<%@include file="navigation.jsp"%>
<div class="jumbotron text-center">
    <h1>Listing des oeuvres en pret</h1>
</div>

<div class="container">
    <a class="btn btn-secondary" href="index.htm" role="button"><span class="glyphicon glyphicon-menu-left"></span> Retour accueil</a>
    <h2>Tableau des oeuvres en pret</h2>
    <div class="container">
        <h3>Liste des oeuvres en pret</h3>
        <table class="table table-hover">
            <tr>
                <th class="col-md-1">Numero</th>
                <th class="col-md-2">Titre</th>
                <th class="col-md-2">Proprietaire</th>
                <th class="col-md-3">Actions</th>
            </tr>

            <c:forEach items="${oeuvrePretList}" var="item">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.titre}</td>
                    <td>${item.proprietaire.prenom} ${item.proprietaire.nom}</td>
                    <td>
                        <a class="btn btn-warning" href="updateOeuvrePret.htm?id=${item.id}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a class="btn btn-danger" href="supprimerOeuvrePret.htm?id=${item.id}">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>

</html>