<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Médiathèque de POLYTECH</a>
            </div>
            <p class="navbar-text">Gestion de l'exposition 2020</p>
            <ul class="nav navbar-nav">
                <li><a href="index.htm"> <span class="glyphicon glyphicon-home"></span> Accueil</a></li>
                <c:if test="${sessionScope.id == null }">
                    <li>
                        <a class="nav navbar-nav navbar-right"  href="login.htm">
                            <span class="glyphicon glyphicon-user"></span>
                            Se Connecter
                            <span class="caret"></span>
                        </a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.id > 0  }">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="glyphicon glyphicon-user"></span>
                            Adhérents
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="updateAdherent.htm"> <span class="glyphicon glyphicon-plus"></span> Ajout Adhérent</a></li>
                            <li><a href="listerAdherent.htm"><span class="glyphicon glyphicon-th-list"></span> Lister les adhérents</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="glyphicon glyphicon-user"></span>
                            Propriétaires
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="updateProprietaire.htm"> <span class="glyphicon glyphicon-plus"></span> Ajout Propriétaire</a></li>
                            <li><a href="listerProprietaire.htm"><span class="glyphicon glyphicon-th-list"></span> Lister les propriétaires</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="glyphicon glyphicon-list"></span>
                            Oeuvres
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li class="dropdown-item"><a href="updateOeuvreVente.htm"> <span class="glyphicon glyphicon-plus"></span> Ajouter une oeuvre en vente</a></li>
                            <li class="dropdown-item"><a href="listerOeuvreVente.htm"><span class="glyphicon glyphicon-th-list"></span> Lister les oeuvres en vente</a></li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item"><a href="updateOeuvrePret.htm"> <span class="glyphicon glyphicon-plus"></span> Ajouter une oeuvre en pret</a></li>
                            <li class="dropdown-item"><a href="listeOeuvrePret.htm"><span class="glyphicon glyphicon-th-list"></span> Lister les oeuvres en pret</a></li>
                        </ul>
                    </li>
                </c:if>
                <li><a href="javascript:fermer()"><span class="glyphicon glyphicon-log-out"></span> Quitter</a></li>
            </ul>
        </div>
    </nav>
</div>