<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<%@include file="header.jsp" %>
<body>
<%@include file="navigation.jsp"%>
<H1> Ajout d'un adhérent </H1>
<form method="post" action="insererAdherent.htm" onsubmit="return teste()">
<div class="col-md-12 well well-md">
    <div class="row" >
        <div class="col-md-12" style ="border:none; background-color:transparent; height :20px;">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Nom de l'adherent : </label>
        <div class="col-md-3">
            <INPUT type="text" name="txtnom" value="${adherent.nom}" id="nom" class="form-control" min="0">
            <input type="hidden" name="id" value="${adherent.id}" />
        </div>

    </div>
    <div class="row" >
        <div class="col-md-12" style ="border:none; background-color:transparent; height :20px;">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Prénom de l'adherent : </label>
        <div class="col-md-3">
            <INPUT type="text" name="txtprenom" value="${adherent.prenom}" id="prenom" class="form-control" min="0">
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12" style ="border:none; background-color:transparent; height :20px;">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Ville de l'adherent : </label>
        <div class="col-md-3">
            <INPUT type="text" name="txtville" value="${adherent.ville}" id="ville" class="form-control" min="0">
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12" style ="border:none; background-color:transparent; height :20px;">
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-ok"></span>
            Ajouter
        </button>

        <a href="index.htm" class="btn btn-default btn-primary">
            <span class="glyphicon glyphicon-remove"></span> Annuler
        </a>
    </div>
</div>
</form>
<%@include file="footer.jsp"%>
</body>

</html>