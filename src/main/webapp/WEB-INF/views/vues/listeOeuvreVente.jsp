<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<%@include file="header.jsp" %>
<body>
<%@include file="navigation.jsp"%>
<div class="jumbotron text-center">
    <h1>Listing des oeuvres en vente</h1>
</div>

<div class="container">
    <a class="btn btn-secondary" href="index.htm" role="button"><span class="glyphicon glyphicon-menu-left"></span> Retour accueil</a>
    <h2>Tableau des oeuvres en vente</h2>
    <div class="container">
        <h3>Liste des oeuvres en vente</h3>
        <table class="table table-hover">
            <tr>
                <th class="col-md-1">Numero</th>
                <th class="col-md-2">Titre</th>
                <th class="col-md-2">Etat</th>
                <th class="col-md-2">Prix</th>
                <th class="col-md-2">Proprietaire</th>
                <th class="col-md-3">Actions</th>
            </tr>

            <c:forEach items="${oeuvreVenteList}" var="item">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.titre}</td>
                    <td>${item.etat}</td>
                    <td>${item.prix}</td>
                    <td>${item.proprietaire.prenom} ${item.proprietaire.nom}</td>
                    <td>
                        <c:if test="${item.etat == 'L'}">
                            <a class="btn btn-success" href="reservationOeuvre.htm?id=${item.id}">
                                <span class="glyphicon glyphicon-plus"></span>
                                Réservation
                            </a>
                        </c:if>
                        <c:if test="${item.reservationList.size() > 0}">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-${item.id}">
                                <span class="glyphicon glyphicon-check"></span>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="modal-${item.id}" tabindex="-1" role="dialog" aria-labelledby="modal-${item.id}-Label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Confirmer la réservation ?
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Adhérent</th>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Etat</th>
                                                        <th scope="col">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${item.reservationList}" var="reservation">
                                                        <tr>
                                                            <td>${reservation.id.adherent.nom} ${reservation.id.adherent.prenom}</td>
                                                            <td><input type="date" value="${reservation.date_reservation}" disabled></td>
                                                            <td>${reservation.statut}</td>
                                                            <td>
                                                                <a href="confirmReservation.htm?idO=${reservation.id.oeuvreVente.id}&idA=${reservation.id.adherent.id}&status=CONFIRMED"
                                                                   class="btn btn-success"
                                                                    ${reservation.statut != "WAITING" ? "disabled" : ""}>
                                                                    <span class="glyphicon glyphicon-ok"></span>
                                                                </a>
                                                                <a href="confirmReservation.htm?idO=${reservation.id.oeuvreVente.id}&idA=${reservation.id.adherent.id}&status=CANCELLED"
                                                                   class="btn btn-danger"
                                                                    ${reservation.statut != "WAITING" ? "disabled" : ""}>
                                                                    <span class="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <a class="btn btn-warning" href="updateOeuvreVente.htm?id=${item.id}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a class="btn btn-danger" href="supprimerOeuvreVente.htm?id=${item.id}">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>

</html>