<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<%@include file="header.jsp" %>
<body>
<%@include file="navigation.jsp"%>
<H1> Ajout d'une oeuvre en vente </H1>
<form method="post" action="insererOeuvreVente.htm" onsubmit="return teste()">
    <div class="col-md-12 well well-md">
        <div class="row" >
            <div class="col-md-12" style ="border:none; background-color:transparent; height :20px;">
            </div>
        </div>
        <div class="row form-group">
            <label for="titre" class="col-md-3 control-label">Titre de l'oeuvre : </label>
            <div class="col-md-3">
                <INPUT type="text" name="titre" value="${oeuvreVente.titre}" id="titre" class="form-control" min="0">
                <input type="hidden" name="id" value="${oeuvreVente.id}" />
            </div>

        </div>

        <div class="row form-group">
            <label for="prix" class="col-md-3 control-label">Prix de l'oeuvre : </label>
            <div class="col-md-3">
                <input class="form-control" id="prix" min="0" name="prix" step="0.01" type="number" pattern="[0-9]" value="${oeuvreVente.prix}">
            </div>
        </div>

        <div class="row form-group">
            <label for="proprietaire" class="col-md-3 control-label">Proprietaire : </label>
            <div class="col-md-3">
                <select name="proprietaire" id="proprietaire">
                    <c:forEach items="${proprietaireList}" var="item">
                        <option value="${item.id}" ${item.id == oeuvreVente.proprietaire.id ? "selected" : ""}>${item.nom} ${item.prenom}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="row form-group">
            <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-ok"></span>
                Ajouter
            </button>

            <a href="listerOeuvreVente.htm" class="btn btn-default btn-primary">
                <span class="glyphicon glyphicon-remove"></span> Annuler
            </a>
        </div>
    </div>
</form>
<%@include file="footer.jsp"%>
</body>

</html>